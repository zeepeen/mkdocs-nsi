# Somme de variables aléatoires

## 1 - Somme de variables aléatoires 

!!! info "Définition 📖"
    Soit $X$ et $Y$, deux variables aléatoires.  La **loi de probabilité** de la somme des variables aléatoire $X + Y$ est donnée par : 
    $$P(X + Y = k) = \sum_{i+y=k} P((X = i) \cap (Y = j)) $$
    Si, de plus, les évènements $X$ et $Y$ sont indépendants, alors :  
    $$P(X + Y = k) = \sum_{i+y=k} P(X = i) \times P(Y = j) $$

    ??? tip "Remarque ❗"
        Si $X$, une variable aléatoire ayant pour valeurs : $1$, $2$ et $Y$, une autre variable aléatoire prenant pour valeurs $-2$, $3$, $4$.  
        Alors, si on veut calculer toutes les sommes où $X + Y = 5$, en sachant que $X$ et $Y$ sont indépendants :  
        $P(X + Y = 5) = \sum_{i + y = 5} P(X = i) \times P(Y = j)$  
        $P(X + Y = 5) = P(X = 1) \times P(Y = 4) + P(X = 2) \times P(Y = 3)$  



??? example "Exemple : Étude d'un jeu de dé"

    Un jeu consiste à lancer un dé équilibré à six faces et à regardes le numéro obtenu. Les régles du jeu sont les suivantes :  
    - Si le numéro est $5$ ou $6~$, on gagne $6$ $€$.  
    - Si le numéro est $1$, $2$ ou $4$, on perd $12$ $€$.  
    - Si le numéro obtenu est $3$, on gagne $15$ $€$.  
    ![Des dés](https://upload.wikimedia.org/wikipedia/commons/6/6a/Dice.jpg){width=300}  
    *Source [^1]*  

    **^^Partie A : On ne qu'une fois^^**

    Dans cette partie, on note $X$ la variable alétoire correspondant au gain (éventuellement négatif) obtenu.


    1. Déterminer la loi de probabilité

    ??? done "Résultat : "
        Le dé étant équilibré, nous sommes dans une situation d'équiprobabilité.  
        Ainsi, chaque valeur du dé ont la même probabilité.
        On fait donc la loi de probabilité $X$.      
        Alors, on peut faire le tableau suivant :   

        | $x_i$ | $-12$ | $6$ |$15$|
        |:--:|:--:|:--:|:--:|
        | $P(X = x_i)$ | $\dfrac{3}{6} = \dfrac{1}{2}$ | $\dfrac{2}{6} = \dfrac{1}{3}$ | $\dfrac{1}{6}$ |

    2. En déduire l'espérance mathématique de $X$ et interpréter le résultat obtenu dans le contexte de l'exemple. Le jeu est-il favorable au joueur ?
    ??? done "Résultat : "
        !!! tip "Aide-mémoire : "
            L'espérance mathématique, notée $E(X)$ est calculée par la formule suivante :  
            $E(X) = x_{1} \times p_{1} + x_{2} \times p_{2} + \dots + x_{i_{max}} \times p_{i_{max}}$  
            Si l'espérance est négative, alors le jeu est **défavorable** au joueur.  
            Si l'espérance est égale à $0$, alors le joueur ne **gagne** rien et ne **perd** rien.
            Enfin, si elle est positive, elle est **favorable** au joueur.  
        D'après l'aide-mémoire, $E(X)$ est égale à $\dfrac{1}{2} \times (-12) + \dfrac{1}{3} \times 6 + \dfrac{1}{6} \times 15 = \dfrac{-12}{2} + \dfrac{6}{3} + \dfrac{15}{6} = -6 + 2 + 2,5 = -1,5$  
        Vu que $-1,5 < 0$, alors le jeu est défavorable au joueur (vu qu'il perd $1,5$ $€$ en moyenne).

    **^^Partie B : on triple les gains (et les pertes)^^**

    Dans cette partie, le responsable du jeu décide de tripler chacun des gains (remportés ou perdus). On note $T$ la variable aléatoire correspondant au gain algébrique obtenu avec ces nouvelles règles.  
    1. Déterminer la loi de probabilité de $T$, puis son espérance.
    ??? done "Résultat : "
        Le dé étant équilibré, nous sommes dans une situation d'équiprobabilité.  
        Ainsi, chaque valeur du dé ont la même probabilité.     
        Alors, on peut faire le tableau suivant :   

        | $x_i$ | $-36$ | $18$ |$45$|
        |:--:|:--:|:--:|:--:|
        | $P(X = x_i)$ | $\dfrac{3}{6} = \dfrac{1}{2}$ | $\dfrac{2}{6} = \dfrac{1}{3}$ | $\dfrac{1}{6}$ |  

        $E(X)$ est égale à $\dfrac{1}{2} \times (-36) + \dfrac{1}{3} \times 18 + \dfrac{1}{6} \times 45 = -18 + 6 + \dfrac{15}{2} = -18 + 6 + 7,5 = -4,5$  
    2. A l'aide de l'énoncé, expliquer pourquoi on peut écrire $T = 3X$

    ??? done "Résultat :"
        Vu que c'est le même jeu dans les deux situations, juste qu'onn multiplie les gains et les pertes par 3.  
        Pour attendre le second jeu à partir du premier, il faut multiplier par 3 le gain ou la perte issu du premier jeu.  
        Ainsi, $T = 3X$.

    3. Quelle relation avons-nous eu avec $E(T)$ et $E(X)$
    ??? done "Résultat :"
        On a eu la relation suivante : $E(T) = 3E(X)$.  
        Si $T = 3X$, alors $E(3X) = 3E(X)$.

    **^^Partie C^^**

    Le jeu présenté dans la partie A étant trop défavorable au joueur, on demande à celui‑ci, à l’issue du premier jet de dé, de jouer une seconde manche dont la règle est la suivante.
    Le joueur relance le dé équilibré : s'il obtient un nombre impair, il gagne $3$ $€$ ; sinon, il perd $2$ $€$.
    On note $Y$ la variable aléatoire correspondant au gain du second jeu, et $Z$ la variable aléatoire correspondant au gain total alors obtenu.

    1. Déterminer la loi de probabilité de $Y$ et justifier que l'on peut écrire $Z = X + Y$

    ??? done "Résultat : "

        $Y$ peut prendre en valeur $-2$ et $3$, de plus, nous sommes en situation d'équiprobabilité alors : 

        | $x_i$ | $-2$ | $3$ |
        |:--:|:--:|:--:|
        | $P(X = x_i)$ | $\dfrac{3}{6} = \dfrac{1}{2}$ | $\dfrac{3}{6} = \dfrac{1}{2}$ | 

        On peut écrire $Z = X + Y$ car le gain total est égal à la somme du gain lors de la première manche (soit $X$) et du gain de la seconde (soit $Y$).

    2.  Pour tout $i ∈ \left\{6;\ −12;\ 15\right\}$, on note $A_{i}$ l’événement « On remporte $i$ euros au cours de la première phase du jeu ».  Pour tout $j ∈ \left\{−2;\ 3\right\}$, on note $B_{j}$ l’événement « On remporte $j$ euros au cours de la seconde phase du jeu ».  Réaliser un arbre pondéré illustrant la situation étudiée.  On admettra que, pour tout $i ∈ \left\{6;\ −12;\ 15\right\}$ et pour tout $j∈\left\{−2;\ 3\right\}$, les événements $A_{i}$ et $B_{j}$ sont indépendants.

    ??? done "Résultat :"
        ![Arbre de probabilité](./images/arbre_probabilité.svg)

    3. Compléter le tableau suivant résumant la loi de probabilité de $Z$ : 

    | $x_i$ | $-14$ | $-9$ | $4$ | $9$ | $13$ | $18$ | 
    |:--:|:--:|:--:|:--:|:--:|:--:|:--:|
    | $P(X = x_i)$ |  |  |  |  |  |  |

    ??? done "Résultat : "

        | $x_i$ | $-14$ | $-9$ | $4$ | $9$ | $13$ | $18$ | 
        |:--:|:--:|:--:|:--:|:--:|:--:|:--:|
        | $P(X = x_i)$ | $\dfrac{1}{2} \times \dfrac{1}{2} = \dfrac{1}{4}$ | $\dfrac{1}{2} \times \dfrac{1}{2} = \dfrac{1}{4}$ | $\dfrac{1}{3} \times \dfrac{1}{2} = \dfrac{1}{6}$ | $\dfrac{1}{3} \times \dfrac{1}{6} = \dfrac{1}{4}$ | $\dfrac{1}{6} \times \dfrac{1}{2} = \dfrac{1}{12}$ | $\dfrac{1}{6} \times \dfrac{1}{2} = \dfrac{1}{12}$ |

    4. Calculer $E(Y)$ et $E(Z)$. Comparer alors $E(Z)$ avec $E(X) + E(Y)$ et déterminer si le jeu est réellement moins désavantageux pour le joueur que lors de la partie A.

    ??? done "Résultat : "
        $E(Y)= -2 \times 0,5 + 3 \times 0,5 = -1 + 1,5 = 0,5$  
        $E(Z) = -14 \times \dfrac{1}{4} + (-9) \times \dfrac{1}{4} + 4 \times \dfrac{1}{6} + 9 \times \dfrac{1}{6} + 13 \times \dfrac{1}{12} + 18 \times \dfrac{1}{12} = -1$  
        $E(X) + E(Y) = 0,5 - 1,5 = -1$
        $E(Z) = E(X) + E(Y)$, le jeu est toujours défavorable pour le joueur mais moins que si on joue uniquement au premier jeu.

    **^^Bilan^^**

    Si $X$ et $Y$ sont deux variables aléatoires et $a$ un nombre réel. Exprimer $E(aX)$ et $E(X+Y)$ en fonction de $E(X)$ et de $E(Y)$

    ??? done "Résultat :"
        $E(aX) = aE(X)$ selon la question 2 de la partie B.  
        $E(X + Y) = E(X) + E(Y)$ selon la question 4 de la partie C.

    *Activité issue du livre scolaire ^^Lelivrescolaire^^, page 360*

[^1]: Source de l'image : [WikiMédia](https://commons.wikimedia.org/wiki/File:Dice.jpg), sous *license CC-BY-SA*.  

## 2 - Espérance et variance de combinaisons linéaires de variables aléatoires

### 1 - Linéairé de l'espérance

!!! info "Propriétés 📖 " 
    1 - $E(aX + b) = aE(X) + b$, avec $a$ et $b \in \mathbb{R}$  
    2 - $E(X + Y) = E(X) + E(Y)$

### 2 - Variance

!!! info "Propriétés 📖 " 
    1 - $V(aX + b) = a^2V(X)$, avec $a$ et $b \in \mathbb{R}$  
    2 - Si $X$ et $Y$ sont deux variables aléatoires indépendantes : $V(X + Y) = V(X) + V(Y)$

??? example "Simplifier des calculs d'espérance ou de variance avec une variable aléatoire de transition : "
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/ljITvCBExVY){.youtube}  

    Une entreprise qui fabrique des roulements à bille fait une étude sur une gamme de billes produites.  
    Le diamètre théorique doit être égal à 1,3 cm mais cette mesure peut être légèrement erronée.  
    L'expérience consiste à tirer au hasard une bille d'un lot de la production et à mesurer son diamètre.   
    
    On considère la variable aléatoire $X$ qui, à une bille choisie au hasard, associe son diamètre.  
    La loi de probabilité de $X$ est résumée dans le tableau suivant : 

    |$x_i$|$1,298$|$1,299$|$1,3$|$1,301$|$1,302$|  
    |:---:|:---:|:---:|:---:|:---:|:---:|  
    |$P(Y = y_i)$| $0,2$| $0,1$ | $0,2$ | $0,4$ | $0,1$ |  

    ^^Calculer l'espérance et l'écart-type de la loi de probabilité de $X$ :^^  

    Pour simplifier les calculs, on définit la variable aléatoire $Y = 1000X - 1300$.  
    La loi de probabilité de $Y$ est alors : 

    |$x_i$|$-2|$-1$|$0$|$1$|$2$|  
    |:---:|:---:|:---:|:---:|:---:|:---:|  
    |$P(Y = y_i)$| $0,2$| $0,1$ | $0,2$ | $0,4$ | $0,1$ |  
    
    Calculons l'espérance et la variance de la loi de probabilité de $Y$ :   
    $E(Y) = -2 \times 0,2 + (-1) \times 0,1 + 1 \times 0,4 + 2 \times 0,1 = 0,1$  
    $V(Y) = 0,2 \times (-2-0,1)^{2} + 0,1 \times (-1-0,1)^{2} +0,2 \times (0-0,1)^{2} + 0,4 \times (1-0,1)^{2} + 0,1 \times (2-0,1)^{2} = 1,69$   

    Avec cela, on en déduit l'espérance et la variance de la loi de probabilité de $X$ :  
    $E(Y) = E(1000X - 1300) = 1000E(X) - 1300$  
    Donc : $E(X) = \dfrac{E(Y) + 1300}{1000} = \dfrac{0,1 + 1300}{1000} = 1,3001$  
    $V(Y) = V(1000X - 1300) = 1000^{2}V(X)$  
    Donc $V(X) = \dfrac{V(Y)}{1000^2} = \dfrac{1,69}{1000^2}$  
    Ainsi, $\sigma(X) = \sqrt{\dfrac{1,69}{1000^2}} = \dfrac{\sqrt{1,69}}{\sqrt{1000^2}} = \dfrac{1,3}{1000} = 0,0013.$  
    Donc, $E(X) = 1,3001$ cm et $\sigma(X) = 0,0013$ cm.


## 3 - Application à la loi binomiale  

### 1 - Echantillion d'une loi de probabilité

??? example "Exemple :"
    ![Image d'illustration d'un composant
    électronique](https://get.pxhere.com/photo/computer-hardware-motherboard-electronic-engineering-electronics-random-access-memory-electronic-component-passive-circuit-component-technology-circuit-component-electrical-network-personal-computer-hardware-electronics-accessory-electronic-device-Solid-state-drive-read-only-memory-computer-component-network-interface-controller-computer-data-storage-I-o-card-circuit-prototyping-hardware-programmer-cpu-microcontroller-Video-memory-computer-accessory-1624111.jpg){width=300}  
    *Source [^2]*

    On étudie la fiabilité d'un composant électronique.  
    On appelle $X$ la variable aléatoire égale à $1$ si le composant électonique ne se détériore pas suite aux tests effectués et $0$ dans le cas contraire.  
    Le fabricant précise que le composant électronique ne subit pas de détériorations suite aux tests dans $99,8 %$ des cas.  
    Dans ce cas, la variable aléatoire $X$ suit la loi de Bernouilli de paramètres $0,998$.  
    On effectue les tests sur un échantillion de $100$ composants électroniques prélevés au hasard dans le stock du fabricant.  
    On peut considérer alors que la liste ( $X_{1},\ X_{2}, \ X_{3}, \dots,\ X_{100}$) forment un échantillon de taille $100$ de variables aléatoires suivant la loi de Bernouilli de paramètre $0,998$.  

!!! note "Définition 📖"  
    Un **échantillon de taille $n$ d'une loi de probabilité** est une liste de $n$ variables aléatoires indépendantes suivant cette loi.

    ??? tip "Aide mémoire sur la loi de Bernouilli et la loi binomiale 😉"
        $X \sim \mathfrak{B}(n, p)$ avec $n \in \mathbb{N}$ et $b \in  ]0; 1[$ signifie que la loi de probabilité $X$ suit la loi de Bernoulli avec les paramètres $n$ et $p$.  
        Ainsi :  
        $E(X) = p$  
        $V(X) = p \times (1 - p)$  
        $\sigma(X) = \sqrt{V(X)} = \sqrt{p \times (1 - p)}$
            
        $X \sim B(n, p)$ avec $n \in \mathbb{N}$ et $b \in  ]0; 1[$ signifie que la loi de probabilité $X$ suit la loi binomiale avec les paramètres $n$ et $p$. 
        Les formules étant les mêmes que celle de la loi de Bernouilli en multipliant par $n$ :  
        $E(X) = n \times p$  
        $V(X) = n \times p \times (1 - p)$
        $\sigma(X) = \sqrt{V(X)} = \sqrt{n \times p \times (1 - p)}$
    !!! note "Propriétés :"
        Soit ($X_{1},\ X_{2}, \ X_{3}, \dots,\ X_{n}$) un échantillon de taille $n$ de variables aléatoires indépendantes suivant une même loi.  
        On pose : $S = X_1 + X_2 + \dots + X_n$, alors on a :   
        1 - $E(S) = E(X_1) + E(X_2) + \dots + E(X_n)$   
        2 - $V(S) = V(X_1) + V(X_2) + \dots + V(X_n)$

[^2]: Source : [PxHere](https://pxhere.com/fr/photo/1624111?utm_content=shareClip&utm_medium=referral&utm_source=pxhere),*license CC-0.*  
??? example "Calculer l’espérance et la variance d’une variable aléatoire en l’exprimant comme somme de variables aléatoires indépendantes."

    ![Image d'illustration d'une usine](https://upload.wikimedia.org/wikipedia/commons/a/a6/Usine_Isoroy_%C3%A0_Lure.jpg){width=300}  
    *Source [^3]*  
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/ljITvCBExVY){.youtube}  
    Un industriel fabrique des plats en verre qui doivent résister à de fortes températures afin de pouvoir être utilisés dans un four de cuisson. Pour vérifier la résistance du plat, on le soumet à une température de 350°C. On a constaté qu'en moyenne, sur un grand nombre de plats testés sortant de l'usine, 1,5% des plats ne supportent pas une telle température et cassent.  
    On choisit au hasard 200 plats produits par l'usine et on effectue, pour chacun d’eux, le test de résistance. On admet qu'étant donné le grand nombre de plats produits par l’usine, ce choix peut être assimilé à un tirage fait de façon indépendante et avec remise.  
    On désigne par $R$ la variable aléatoire comptant le nombre de plats résistants au test. Calculer $E(R)$ et $\sigma(R)$. Interpréter les résultats.  

    On appelle $X$ la variable aléatoire égale à $1$ si le plat résiste au test et $0$ dans le cas contraire.  
    $X$ suit une loi de Bernouilli de paramètre $p = 0,985$. En effet, d'après l'énoncé, $E(X) = 1 - 0,015 = 0,985$.  
    Par conséquent, $V(X) = 0,985 \times (1 - 0,985) = 0,014775$.  
      
    On peut considérer alors la liste ($X_1, X_2, X_3, \dots, X_{200}$) forment un échantillion de taille $200$ de variables aléatoires suivant la loi de Bernouilli de paramètre $0,985$.  
    Et on a :  
    - $E(R) = E(X_1) + E(X_2) + \dots + E(X_{200}) = 200 \times 0,985 = 197$  
    - $V(R) = V(X_1) + V(X_2) + \dots + V(X_{200}) = 200 \times 0,014775 \simeq 2,995$ car les variables aléatoires constituants la liste sont indépendants.  
    Ainsi : $\sigma(R) = \sqrt{2,995} \simeq 1,72$.  
    En moyenne, sur un grand nombre d'échantillons de 200 plats, on peut espérer trouver 197 plats résistants avec un écart-type proche de 1,72.  

[^3]: Source: [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/a/a6/Usine_Isoroy_%C3%A0_Lure.jpg), sous *license CC-BY-SA*.

### Echantillon de la loi de Bernouilli

!!! note "Propriété 📖"
    Soit ($X_1, X_2, \dots, X_n$) un échantillon de taille $n$ de la loi de Bernouilli de paramètre $p$.  
    La variable aléatoire $S = X_1 + X_2 + \dots + X_n$ suit la loi binomiale de paramètres $n$ et $p$.  

??? example "Exemple : "
    En reprenant l'exemple donné au paragraphe *III.1*, la variable aléatoire $S = X_1 + X_2 + \dots + X_{100}$ suit la loi binomiale de paramètres $n = 100$ et $p = 0,998$.  

### Espérance, variance et écart-type de la loi binomiale  

!!! note "Démonstration au programme de Terminale : "
    [*Cet exercice a été effectué par sur Youtube.*</br>](https://youtu.be/ljWJfGLRgJE){.youtube}*  
    Soit ($X_1, X_2, \dots, X_n$) un échantillion de taille $n$ de la loi de Bernouilli de paramètre $p$.  
    Donc, on a :  
    $E(S) = E(X_1 + X_2 + \dots + X_n) = E(X_1) + E(X_2) + \dots + E(X_n) = p + p + \dots + p = n \times p$  
    $V(S) = V(X_1 + X_2 + \dots + X_n) = V(X_1) + V(X_2) + \dots + V(X_n)$  $V(S) = p(1 - p) + p(1 - p) \dots + p(1 - p) = n \times p(1 - p)$  *(toutes les variables aléatoires de l'échantillon sont indépendantes).* 


??? example "Calculer l'espérance, la variance et l'écart-type pour une loi binomiale : "
    [*Cet exercice a été effectué par Yvan Monka*</br>](https://youtu.be/MvCZw9XIZ4Q){.youtube}   
    On lance $5$ fois de suite un dé à six faces.
    On considère comme succès le fait d'obtenir $5$ ou $6$.  
    On considère la variable aléatoire $S$ donnant le nombre de succès.  
      
    ^^Calculer $E(S)$, $V(S)$ et $\sigma(S)$^^  

    La variable aléatoire $S$ suit la loi binomiale de paramètres $p = \dfrac{1}{3}$ et $n = 5$.  
    $E(S) = 5 \times \dfrac{1}{3} = \dfrac{5}{3} \simeq 1,7$  
    $V(S) = 5 \times \dfrac{1}{3} \times \dfrac{2}{3} = \dfrac{10}{9}$  
    $\sigma(S) = \sqrt{\dfrac{10}{9}} = \dfrac{\sqrt{10}}{3}$  
    En moyenne, on peut espérer obtenir, environ, 1,7 fois un 5 ou 6, en 5 lancers.


*Ce cours est largement inspiré par celui de Mme CASTELLI.*