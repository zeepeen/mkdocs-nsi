# Primitives d'une fonction continue

## Définition d'une équation différentielles

!!! note "Définition 📖"
    Une **équation différentielle** est une égalité liant une fonction inconnue $y$ de la variable $x$, ses dérivées successives $y'$, $y''$, ... et éventuellement d'autres fonctions (constantes, $f$, ...)  
    On appelle solution d'une équation différentielle, toute fonction dérivable vérifiant l'égalité.  
    Résoudre une équation différentielle, c'est trouver toutes les fonctions vérifiant l'égalité.  

    ??? example "Exemples :"  
        1. L'équation $f'(x) = 5$ peut se noter $y' = 5$ en considérant que $y$ est une fonction inconnue qui dépend de $x$.  
        Dans ce cas, une solution de cette équation est $y = 5x$. En effet, $(5x)' = 5$.  
        1. Une solution de l'équation $y = 5x$ est $y' = x^2$.  
        Pour une équation différentielle, la solution n'est pas habituellement unique.  
        Par exemple, $y = x^2 + 1$ est une solution de l'équation différentielle.  
        En effet, $(x^2 + 1)' = 2x$.  

    !!! note "Remarques ❗"
        - La dérivée est associée à un taux de variation, quotient des variations de $y$ sur les variations de $x$, d'où le terme de *différentiel*.  
        -On peut être amené à utiliser l'écriture différentielle $y' = \dfrac{dy}{dt}$ ou $y' = \dfrac{dy}{dx}$.  

## Équation différentielle du type $y' = f$

!!! note "Définition 📖"
    Soit $f$ une fonction définie sur une intervalle I de $\mathbb{R}$.  
    On dit que le fonction $g$ est une **solution** de l'équation différentielle $y' = f$ sur I, si et seulement si, $g$ est dérivable sur I et pour tout réel $x$ de I, on a : $g'(x) = f(x)$.  

??? example "Vérifier qu'une fonction est solution d'une équation différentielle"  
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*</br>](https://youtu.be/B9n_AArwjKw){.youtube}
    Prouver que la fonction $g$ est définie sur l'intervalle I, $]0; \, +\infty[$, par $g(x) = 3x^{2} + \ln(x)$ est solution de l'équation différentielle $y' = 6x + \dfrac{1}{x}$.  
    Pour tout $x \in$ I, on a :  
    $g'(x) = 3 \times 2x + \dfrac{1}{x} = 6x + \dfrac{1}{x}$.  

## Primitive d'une fonction

??? example "Exemples :"
    On considère les fonctions suivantes :  
    $f(x) = 2x + 3$ et $F(x) = x^{2} + 3x - 1$ où $f:\mathbb{R} \rightarrow \mathbb{R}$ et $F:\mathbb{R} \rightarrow \mathbb{R}$.  
    On constate que $F'(x) = 2x + 3 = f(x)$.  
    $F$ est donc solution de l'équation différentielle $y' = f$.  
    On dit dans ce cas que $F$ est la primitive de $f$ sur $\mathbb{R}$.  

!!! note "Définition 📖"
    $f$ est une fonction continue sur un intervalle I.  
    On appelle **primitive** de $f$ sur I, une fonction $F$ dérivable sur I telle que $F' = f$.
    ??? tip "Remarque ❗"
        Dans ces conditions, on a l'équivalence :  
        "$F$ a pour dérivée $f$" et "$f$ a pour primitive $F$".

??? example "Exemple :"
    $F(x) = \dfrac{x^2}{2}$ est une primitive de $f(x) x$ car $F'(x) = f'(x)$, pour tout réel $x$.

!!! note "Propriété 📖"
    Deux primitives d'une même fonction continue sur un intervalle diffèrent d'une constante.

!!! note "Démonstration au programme de Terminale :"
    Soit $F$ et $G$ deux primitives sur la fonction $f$ sur I.  
    Alors : $F'(x) = f(x)$ et $G'(x) = f(x)$.  
    Donc : $F'(x) = G'(x)$, soit $F'(x) - G'(x) = 0$, soit encore $(F - G)'(x) = 0$.  
    La fonction $F - G$ possède une dérivée nulle sur I, elle est donc constante sur I.  
    On nomme $C$ cette constance. Ainsi, $F(x) - G(x) = C$, pour tout $x$ de I.  
    On en déduit que les deux primitives de $f$ diffèrent d'une constante.

!!! note "Propriété 📖"
    $f$ est une fonction continue sur un intervalle I$.  
    Si $F$ est une primitive de $f$ sut I alors pour tout réel $C$, la fonction $x \rightarrow F(x) + C$ est une primitive de $f$ sur I.
    ??? note "Démonstration non au programme de Terminale :"
        $F$ est une primitive de $f$.  
        On pose : $G(x) = F(x) + C$.  
        $G'(x) = F'(x) + 0 = F'(x) = f(x)$.  
        Donc $G$ est une primitive de $f$.  

??? example "Exemple :"
    $F(x) = \dfrac{x^2}{2}$ est une primitive de $f(x) = x$.  
    Donc, toute fonction de la forme $G_{C}(x) = \dfrac{x^2}{2} + C$, avec $C \in \mathbb{R}$ est une primitive de $f$.

!!! note "Propriété 📖"
    Tout fonction continue sur un intervalle admet des primitives sur cet intervalle.
    ??? tip "Remarque ❗"
        Bien que l'existance étant assurée, la forme explicite d'une primitive n'est pas toujours connue.  
        Par exemple, la fonction $f(x) = e^{-x^{2}}$ ne possède pas de primitive sous forme explicite.

!!! example "Recherche d'une primitive particulière"
    [*Cet exercice a été effectué par Yvan Monka sur Youtube*](https://youtu.be/-q9M7oJ9gkI)  
    Soit la fonction $f$ définie sur $\mathbb{R}^{*}$ par $f(x) = \dfrac{e^{2x}(2x - 1)}{x^2}$.  

    1. Démontrer que la fonction $F$ définie sur $\mathbb{R}^{*}$ par $F(x) = \dfrac{e^{2x}}{x}$ est une primitive de $f$.  
    2. Déterminer la primitive de la fonction $f$ qui s'annule en $x = 1$.  
    1) La fonction $F$ est une primitive de $f$, si $F' = f$.  
    $F'(x) = \dfrac{2e^{2x}x - e^{2x}}{x^2} = \dfrac{e^{2x}(2x - 1)}{x^2} = f(x).  
    2) Toutes les primitives de $f$ sont de la forme : $G(x) = F(x) + C$ où $C$ est un nombre réel.  
    On cherche la primitive de la fonction $f$ qui s'annule en $x = 1$, soit : $G(1) = 0$.  
    Donc : $F(1) + C = 0$.  
    Soit : $\dfrac{e^{2 \times 1}}{1} + C = 0$.  
    $C = -e^{2}$  
    La primitive de la fonction $f$ qui s'annule en $x = 1$ est $G$ telle que :  
    $G(x) = F(x) - e^2 = \dfrac{e^{2x}}{x} - e^{2x}$  

## Primitives de fonctions usuelles

|Fonctions|Primitives|Intervalle|
|:-:|:-:|:-:|
|$a \in \mathbb{R}$ | $ax$ | $\mathbb{R}$|
|$x^{n}$, $n \in \mathbb{N}$ | $\dfrac{1}{n+1} \; \times \; x^{n+1}$ | $\mathbb{R}$|
|$\dfrac{1}{x}$ | $\ln(x)$ | $\left]0 \: +\infty \right[$|
|$\dfrac{1}{x^2}$ | $-\dfrac{1}{x}$ | $\mathbb{R}^{*}$|
|$\dfrac{1}{\sqrt{x}}$ | $2\sqrt{x}$ | $\left]0 \: +\infty \right[$|
|$e^{x}$ | $e^{x}$ | $\mathbb{R}$|

## Linéarité des primitives

!!! note "Propriété 📖"
    $f$ et $g$ sont deux fonctions continues sur un intervalle I.  
    Si $F$ est une primitive de $f$ et $G$ est une primitive de $g$ sur I alors :  
    - $F$ et $G$ est une primitive de $f + g$.  
    - $kF$ est une primitive de $kf$ avec $k \in \mathbb{R}$. 

    ??? tip "Démonstrations non au programme de Terminale :"
        - $(F + G)' = F' + G' = f + g$  
        - $(kF)' = kF' = kf$

## Opérations et fonctions composées 

$u$ est une fonction dérivable sur un intervalle I.  

|Fonctions|Primitives|Intervalle|
|:-:|:-:|:-:|
|$u' \times u^{n}$|$\dfrac{1}{n + 1} \times u^{n+1}$|$n\mathbb{Z}_{-1}$|
|$\dfrac{u'}{\sqrt{u}}$|$2\sqrt{u}$|$u \geq 0$|
|$u'e^{u}$|$e^{u}$|$\mathbb{R}$|
|$\dfrac{u'}{u}$|$\ln(u)$|$u > 0\$|
|$u' \times \cos(u)$|$\sin(u)$|$\mathbb{R}$|
|$u' \times \sin(u)$|$-\cos(u)$|$\mathbb{R}$|  

??? example "Recherche d'une primitive particulière : "
    Exercice fait par Yvan Monka :   
        - [Partie 1](https://youtu.be/GA6jMgLd_Cw)  
        - [Partie 2](https://youtu.be/82HYI4xuClw)  
        - [Partie 3](https://youtu.be/gxRpmHWnoGQ)  
        - [Partie 4](https://youtu.be/iiq6eUQee9g)  
    Dans chaque cas, déterminer une primitive $F$ de la fonction $f$ sur un intervalle I.  
    a) $f(x) = x^{3} - 2x$ sur I $= \mathbb{R}$  
    b) $f(x) = (2x - 5)(x^{2} - 5x + 4)^2$ sur I $= \mathbb{R}$  
    c) $f(x) = x^{2}e^{x^{3}}$ sur I $= \mathbb{R}$  
    d) $f(x) =$
    e)
    f)

    
